Readme file for the Feedback module for Drupal 7
---------------------------------------------------

Features:
--------
feedback_or_survey.module is useful for surveys or feedbacks.
Using this module we can create surveys or feedbacks.
We can assign role for surveys or feedbacks.
Able to see the report in graphical form.
Three different types of questions can create for survey or feedback(single answers,multiple answers,text based answers).

Installation:
------------
  Installation is like with all normal drupal modules:
  extract the 'feedback_or_survey' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Dependencies:
------------
  The basic User Feedback module has no dependencies, nothing special is required.
Menus:
------
1.feedback/create (for creation of feedbacks or surveys)
2.feedback/questions/create (for creation of questions for feedback or surveys)
3.feedback/surveys/list (view feedback or surveys report)
4.feedback/take-your-servey (to take feedbacks or surveys by user)

Author:
-------
Venkat N
venkatwinfo@gmail.com