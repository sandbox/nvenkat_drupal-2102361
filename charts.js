google.load("visualization", "1", {packages:["corechart"]});
jQuery.noConflict();
jQuery(document).ready(function($) {
window.drawPieChart = function(){
$('.pie_chart').each(function( ){
var divid = this.id;
var t=$('#'+this.id).attr("title");
			qid=divid.split('_');
			   var jsonData = $.ajax({
				  url: "http://cportal.drupal.com/feedback/chartdata/qid/"+qid[1],
				  
				  async: false
				  }).responseText;
				  var data = new google.visualization.DataTable(jsonData);
        var options = {
          title: t,
		  sliceVisibilityThreshold:0

        };

        var chart = new google.visualization.PieChart(document.getElementById(this.id));
        chart.draw(data,options);
});		
   
}
});
